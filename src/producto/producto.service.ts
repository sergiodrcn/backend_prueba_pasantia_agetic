import { Injectable } from '@nestjs/common';
import { Producto } from '../entity/producto.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class ProductoService {


    constructor(
        @InjectRepository(Producto) private productosRepo: Repository<Producto>
    ) { }

    private productos: Producto[] =
        [{
            id: 1,
            nombre: 'Sergio',
            cantidad: 1,
            precioReferencial: 10,
            fechaRegistro: new Date('2022-03-15T12:30:00Z'),
            fechaActualizacion: new Date('2022-03-15T12:30:00Z')

        }]



    findAll() {
        return this.productosRepo.find();
    }
    findOne(idx: number) {
        // return this.productosRepo.findOne(idx);
    }
    create(body: any) {
        const newProducto = new Producto();
        newProducto.nombre = body.nombre;
        newProducto.cantidad = body.cantidad;
        newProducto.precioReferencial = body.precioReferencial;
        newProducto.fechaRegistro = body.fechaRegistro;
        newProducto.fechaActualizacion = body.fechaActualizacion;
        return this.productosRepo.save(newProducto);
        //  return body;
    }

    getProductos() {
        return this.productos
    }
    postProductos() { }
    putProductos() { }
    deleteProductos() { }

    async update(id: number, body: any) {
        const task = await this.productosRepo.findOneById(id);
        this.productosRepo.merge(task, body);
        return this.productosRepo.save(task);
    }

    async remove(id: number) {
        await this.productosRepo.delete(id);
        return true;
    }

}
