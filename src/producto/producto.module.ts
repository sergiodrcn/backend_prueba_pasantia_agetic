import { Module } from '@nestjs/common';
import { TypeOrmModule} from '@nestjs/typeorm'
import { ProductoController } from './producto.controller';
import { ProductoService } from './producto.service';
import { Producto } from '../entity/producto.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Producto])
  ],
  controllers: [ProductoController],
  providers: [ProductoService]
})
export class ProductoModule {}
