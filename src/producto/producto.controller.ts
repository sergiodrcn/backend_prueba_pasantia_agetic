import { Controller, Get, Post, Body, Put, Delete, Param } from '@nestjs/common';
import { ProductoService } from './producto.service'; 
@Controller('productos')
export class ProductoController {


    constructor(private productosService : ProductoService){}

    @Get()
    getProductos(){
        return this.productosService.findAll();
    }
    @Post()
    postProductos(@Body() body: any){
        return this.productosService.create(body);
    }
    @Put(':id')
    update(@Param('id') id: number, @Body() body: any) {
      return this.productosService.update(id, body);
    }
  
    @Delete(':id')
    delete(@Param('id') id: number) {
      return this.productosService.remove(id);
    }
}
