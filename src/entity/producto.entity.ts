import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm'

@Entity()
export class Producto {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nombre: string;

    @Column()
    cantidad: number;

    @Column()
    precioReferencial: number;

    @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
    fechaRegistro: Date;

    @Column({ type: 'timestamp', onUpdate: 'CURRENT_TIMESTAMP', nullable: true })
    fechaActualizacion: Date;

}

const producto = new Producto()
